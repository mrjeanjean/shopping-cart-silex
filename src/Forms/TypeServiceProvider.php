<?php


namespace Forms;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class TypeServiceProvider implements ServiceProviderInterface
{
    protected $database;

    public function register(Container $app)
    {
        $app['order.type'] = function () {
            return new OrderType();
        };

        $app['meal.type'] = function () {
            return new MealType();
        };

        $app['resto.type'] = function () {
            return new RestoType();
        };
    }
}