<?php


namespace Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints as Assert;

class OrderType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname', TextType::class, array(
            'constraints' => array(
                new Assert\NotBlank()
            )
        ));

        $builder->add('lastname', TextType::class, array(
            'constraints' => array(
                new Assert\NotBlank()
            )
        ));

        $builder->add('address', TextType::class, array(
            'constraints' => array(
                new Assert\NotBlank()
            )
        ));

        $builder->add('address_add', TextType::class, array(
            'required' => false
        ));

        $builder->add('zip', IntegerType::class, array(
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Type("integer")
            )
        ));

        $builder->add('city', TextType::class, array(
            'constraints' => array(
                new Assert\NotBlank()
            )
        ));

        $builder->add('email', TextType::class, array(
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Email()
            )
        ));

        $builder->add('password', PasswordType::class, array(
            'constraints' => array(
                new Assert\NotBlank()
            )
        ));

        $builder->add('password_confirm', PasswordType::class, array(
            'constraints' => array(
                new Assert\NotBlank(),
            )
        ));

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();

            if ($data['password'] != $data['password_confirm']) {
                $error = new FormError("password_similar");
                $form->get('password')->addError($error);
                $form->get('password_confirm')->addError($error);
            }
        });
    }

    public function getParent()
    {
        return 'resto.type';
    }
}