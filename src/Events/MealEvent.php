<?php

namespace Events;

use Models\Meal;
use Symfony\Component\EventDispatcher\Event;

class MealEvent extends Event
{
    const MEAL_ADDED = "meal.added";

    private $meal;
    private $quantity;

    public function __construct(Meal $meal, $quantity)
    {
        $this->meal = $meal;
        $this->quantity = $quantity;
    }

    public function getMeal()
    {
        return $this->meal;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
}