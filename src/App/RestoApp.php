<?php

namespace App;

use Controllers\RestoControllerProvider;
use Forms\TypeServiceProvider;
use Services\RestoServiceProvider;
use Silex\Application as SilexApplication;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;


class RestoApp extends SilexApplication
{
    protected $app;

    public function __construct()
    {
        parent::__construct();
        $this->app = $this;

        $this->app['debug'] = true;
        $this->registerProviders();
        $this->registerRoutes();
    }

    protected function registerProviders()
    {
        $app = $this->app;

        $this->app->register(new ServiceControllerServiceProvider());

        $this->app->register(new DoctrineServiceProvider(), array(
            'db.options' => array(
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'restaurant',
                'user' => 'root',
                'password' => 'root',
                'charset' => 'utf8mb4',
            ),
        ));

        $this->app->register(new TwigServiceProvider(), array(
            'twig.path' => __DIR__ . '/../../templates',
        ));

        $this->app->register(new ValidatorServiceProvider());

        $this->app->register(new AssetServiceProvider(), array(
            'assets.named_packages' => array(
                'css' => array('base_path' => '../web/css/'),
                'meals' => array('base_path' => '../web/images/meals/'),
                'images' => array('base_path' => '../web/images/'),
                'js' => array('base_path' => '../web/js/'),
            ),
        ));

        $this->app->register(new LocaleServiceProvider(), array(
            'locale' => 'fr'
        ));

        $this->app->register(new TranslationServiceProvider(), array(
            'translator.domains' => array(),
        ));
        $this->app->register(new FormServiceProvider());

        $this->app->register(new HttpFragmentServiceProvider());

        $this->app->register(new SessionServiceProvider(), array(
            'cookie_lifetime' => 0
        ));

        $this->app->register(new RestoServiceProvider(), array(
                'resto.database' => $this->app['db'],
                'resto.session' => $this->app['session']
            )
        );

        $this->app->register(new RestoControllerProvider());

        $this->app->register(new TypeServiceProvider());

        $app->extend('form.types', function ($types) use ($app) {
            $types[] = 'order.type';
            $types[] = 'meal.type';
            $types[] = 'resto.type';
            return $types;
        });

        $app->extend('translator', function(Translator $translator) {
            $translator->addLoader('yaml', new YamlFileLoader());
            $translator->addResource('yaml', __DIR__ . '/../../translations/fr.yml', 'fr');
            $translator->addResource('yaml', __DIR__ . '/../../translations/forms.fr.yml', 'fr', 'forms');
            $translator->addResource('yaml', __DIR__ . '/../../translations/validators.fr.yml', 'fr', 'validators');

            return $translator;
        });

        $this->app->register(new WebProfilerServiceProvider(), array(
            'profiler.cache_dir' => __DIR__ . '/../../cache/profiler',
            'profiler.mount_prefix' => '/_profiler', // this is the default
        ));
    }

    protected function registerRoutes()
    {
        // Home
        $this->app->get('/', 'home.controller:index')->bind('homepage');

        // Meal
        $this->app->match('/meal/{id}', 'meal.controller:showMeal')->bind('meal');
        $this->app->get('/add-meal/{meal_id}/{quantity}', 'meal.controller:addMeal')->bind('add-meal-fast');
        $this->app->get('/remove-meal/{meal_id}', 'meal.controller:removeMeal')->bind('remove-meal');

        // Cart
        $this->app->get('/cart', 'cart.controller:showCart')->bind('cart');

        // Order
        $this->app->match('/order', 'order.controller:showOrder')->bind('order');

        // Validation Order
        $this->app->get('/order/success', 'order.controller:successOrder');
    }
}