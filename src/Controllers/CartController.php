<?php

namespace Controllers;

use Services\CartService;
use Services\UserService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class CartController
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function showCart(Application $app)
    {
        if ($this->cartService->isEmpty()) {
            $app['session']->getFlashBag()->add('message', 'Votre panier est vide');
            return $app->redirect($app['url_generator']->generate('homepage'));
        }

        return $app['twig']->render('cart.twig', array(
                "title" => "Panier",
                "meals" => $this->cartService->getCartContent(),
                "total" => $this->cartService->getTotal(),
                "tax" => $this->cartService->getTax()
            )
        );
    }

    public function miniCart(Application $app)
    {
        $products_number = $this->cartService->countProducts();
        $products = $this->cartService->getCartContent();
        $total = $this->cartService->getTotal();

        return $app['twig']->render('mini-cart.twig', array(
                "products_number" => $products_number,
                "products" => $products,
                "total" => $total
            )
        );
    }

}