<?php

namespace Controllers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class RestoControllerProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app["home.controller"] = function () use ($app) {
            return new HomeController($app["meal.service"]);
        };

        $app["meal.controller"] = function () use ($app) {
            return new MealController($app["meal.service"], $app["cart.service"]);
        };

        $app["cart.controller"] = function () use ($app) {
            return new CartController($app["cart.service"], $app['user.service']);
        };

        $app["order.controller"] = function () use ($app){
            return new OrderController($app["cart.service"], $app['user.service']);
        };
    }
}