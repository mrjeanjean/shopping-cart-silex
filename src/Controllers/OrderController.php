<?php
/**
 * Created by PhpStorm.
 * User: Gaëtan
 * Date: 11/07/2017
 * Time: 12:06
 */

namespace Controllers;


use Forms\MealTypeForm;
use Forms\OrderTypeForm;
use Services\CartService;
use Services\UserService;
use Silex\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class OrderController
{
    protected $cartService;
    protected $userService;

    public function __construct(CartService $cartService, UserService $userService)
    {
        $this->cartService = $cartService;
        $this->userService = $userService;
    }

    public function showOrder(Request $request, Application $app)
    {
        if ($this->cartService->isEmpty()) {
            $app['session']->getFlashBag()->add('message', 'Votre panier est vide');
            return $app->redirect($app['url_generator']->generate('homepage'));
        }

        /**
         * @var $formFactory FormFactory
         */
        $formFactory = $app['form.factory'];

        $form = $formFactory->create('order.type');

        if ($form->handleRequest($request)->isValid()) {
            $data = $form->getData();

            try {
                $user_id = $this->userService->saveUser(array(
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname'],
                    'email' => $data['email'],
                    'password' => $data['password'],
                    'address' => $data['address'] . " " . $data['address_add'],
                    'city' => $data['city'],
                    'zip' => $data['zip']
                ));

                $app['order.service']->saveOrder($user_id);

                return $app->redirect("order/success");

            }catch(Exception $exception){
                $form->addError(new FormError($exception->getMessage()));
            }

        }

        return $app['twig']->render('order.twig', array(
                "title" => "Commande",
                "meals" => $this->cartService->getCartContent(),
                "total" => $this->cartService->getTotal(),
                "tax" => $this->cartService->getTax(),
                "form" => $form->createView()
            )
        );
    }

    public function saveOrder(Request $request, Application $app)
    {

    }

    public function successOrder(Application $app)
    {
        $this->cartService->emptyCart();
        return $app['twig']->render('success.twig', array("title" => "Confirmation commande"));
    }
}