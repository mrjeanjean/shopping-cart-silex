<?php

namespace Controllers;

use Doctrine\MongoDB\LoggableCursor;
use Events\MealEvent;
use Services\CartService;
use Services\MealService;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class MealController
{
    protected $mealService;
    protected $cartService;

    public function __construct(MealService $mealService, CartService $cartService)
    {
        $this->mealService = $mealService;
        $this->cartService = $cartService;
    }

    public function showMeal(Request $request, Application $app)
    {
        $meal = $this->mealService->getMeal($request->get('id'));
        /**
         * @var $formFactory FormFactory
         */
        $formFactory = $app['form.factory'];

        $form = $formFactory->create('meal.type', null, array(
            'meal' => $meal
        ));

        if ($form->handleRequest($request)->isValid()) {
            $data = $form->getData();
            $this->cartService->addMeal($meal, $data['quantity']);

            $app['dispatcher']->dispatch(MealEvent::MEAL_ADDED, new MealEvent($meal, $data['quantity']));

            return $app->redirect($app['url_generator']->generate('homepage'));
        }

        return $app['twig']->render("meal.twig", array(
            "meal" => $meal,
            'form' => $form->createView()
        ));
    }

    public function addMeal(Request $request, Application $app)
    {
        $meal = $this->mealService->getMeal($request->get('meal_id'));
        $this->cartService->addMeal($meal, $request->get('quantity'));

        $app['dispatcher']->dispatch(MealEvent::MEAL_ADDED, new MealEvent($meal, $request->get('quantity')));

        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    public function removeMeal(Request $request, Application $app)
    {
        $this->cartService->removeMeal($request->get('meal_id'));
        return $app->redirect($app['url_generator']->generate('cart'));
    }
}

?>