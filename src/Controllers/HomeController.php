<?php

namespace Controllers;

use Services\MealService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class HomeController
{
    protected $mealService;

    public function __construct(MealService $mealService)
    {
        $this->mealService = $mealService;
    }

    public function index(Request $request, Application $app){
        $meals = $this->mealService->getAll();
        return $app['twig']->render('home.twig', array(
            "title" => "Accueil",
            "meals" => $meals
        ));
    }
}