<?php

namespace Services;

use Doctrine\DBAL\Connection;
use Events\MealEvent;
use Models\Meal;
use Silex\Application;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MealService
{
    protected $database;

    public function __construct(Connection $database)
    {
        $this->database = $database;
    }

    public function getMeal($id)
    {
        $meal = $this->database->fetchAssoc(
            "SELECT * FROM meal WHERE id = ?",
            array($id)
        );

        return new Meal($meal['Id'], $meal['Name'], $meal['SalePrice'], $meal['Photo'], $meal['QuantityInStock'], $meal['Description']);
    }

    public function getAll()
    {
        return $this->database->fetchAll(
            "SELECT * FROM meal"
        );
    }

}

?>