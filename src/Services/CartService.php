<?php

namespace Services;

use Models\Meal;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;

class CartService
{
    protected $session;
    protected $taxRate = 20;

    public function __construct(Session $session)
    {
        $this->session = $session;

        if ($this->session->get('cart') === null) {
            $this->session->set('cart', array());
        }
    }

    public function addMeal(Meal $meal, $quantity = 1)
    {
        $cart = $this->session->get('cart');

        $product = array(
            'meal' => $meal,
            'quantity' => $quantity,
            'price' => $meal->getPrice()
        );

        if(array_key_exists($meal->getId(), $cart)){
            $product['quantity'] +=  $cart[$meal->getId()]['quantity'];
        }

        $cart[$meal->getId()] = $product;

        $this->session->set('cart', $cart);
    }

    public function getCartContent(){
        return $this->session->get('cart');
    }

    public function getTotal(){
        $cart = $this->session->get('cart');
        $total = 0;
        foreach ($cart as $meal) {
            $total += $meal['meal']->getPrice() * $meal['quantity'];
        };
        return $total;
    }

    public function emptyCart(){
        $this->session->set('cart', array());
    }

    public function isEmpty(){
        if(empty($this->session->get('cart'))){
            return true;
        }

        return false;
    }

    public function removeMeal($id){
        $cart = $this->session->get('cart');

        if(array_key_exists($id, $cart)){
            $meal = $cart[$id]['meal'];
            unset($cart[$id]);
            $this->session->getFlashBag()->add('message', "Le produit \"{$meal->getName()}\" a été supprimé avec succès");
            $this->session->set('cart', $cart);
        }
    }

    public function getTax()
    {
        return $this->getTotal() * $this->taxRate / 100;
    }

    public function countProducts()
    {
        $cart = $this->session->get('cart');
        $count = 0;
        foreach ($cart as $meal) {
            $count += $meal['quantity'];
        };
        return $count;
    }
}