<?php


namespace Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\EventListenerProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RestoServiceProvider implements ServiceProviderInterface, EventListenerProviderInterface
{
    protected $database;

    public function register(Container $app)
    {
        $app['meal.service'] = function ($app) {
            return new MealService($app['resto.database']);
        };

        $app['cart.service'] = function ($app) {
            return new CartService($app['resto.session']);
        };

        $app['notification.service'] = function ($app) {
            return new NotificationService($app['resto.session']);
        };

        $app['order.service'] = function ($app) {
            return new OrderService($app['resto.database'], $app['cart.service']);
        };

        $app['user.service'] = function ($app) {
            return new UserService($app['resto.database']);
        };
    }

    public function subscribe(Container $app, EventDispatcherInterface $dispatcher)
    {
        $dispatcher->addSubscriber($app['notification.service']);
    }
}