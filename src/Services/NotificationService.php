<?php


namespace Services;

use Events\MealEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class NotificationService implements EventSubscriberInterface
{
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return array(
            MealEvent::MEAL_ADDED => 'onMealAdded'
        );
    }

    public function onMealAdded(MealEvent $mealEvent)
    {
        $this->session->getFlashBag()->add('message', $mealEvent->getQuantity() . ' "' . $mealEvent->getMeal()->getName() . '" ajouté(s) à votre panier');
    }
}