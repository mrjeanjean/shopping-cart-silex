-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 08 Juillet 2017 à 10:00
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `restaurant`
--
CREATE DATABASE IF NOT EXISTS `3w_restaurant` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `3w_restaurant`;

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

CREATE TABLE `3w_booking` (
  `Id` int(11) NOT NULL,
  `BookingDate` date NOT NULL,
  `BookingTime` time NOT NULL,
  `NumberOfSeats` tinyint(4) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `CreationTimestamp` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `booking`
--

INSERT INTO `3w_booking` (`Id`, `BookingDate`, `BookingTime`, `NumberOfSeats`, `User_Id`, `CreationTimestamp`) VALUES
(10, '2016-03-16', '18:45:00', 8, 13, '2016-03-09 10:28:45'),
(11, '2016-04-20', '20:30:00', 3, 13, '2016-03-09 10:34:49'),
(12, '2016-05-11', '22:15:00', 2, 13, '2016-03-09 11:10:29'),
(13, '2016-04-17', '19:30:00', 2, 13, '2016-03-09 11:12:18'),
(14, '2016-03-28', '12:00:00', 1, 13, '2016-03-09 11:12:41'),
(16, '2016-03-14', '13:00:00', 2, 13, '2016-03-09 11:15:46'),
(17, '2016-03-31', '12:00:00', 12, 13, '2016-03-09 11:16:43'),
(18, '2016-05-05', '12:15:00', 4, 13, '2016-03-09 12:33:28'),
(19, '2016-07-19', '19:30:00', 6, 13, '2016-03-09 12:36:10'),
(20, '2016-05-18', '12:00:00', 2, 13, '2016-03-09 12:49:30'),
(21, '0000-00-00', '17:00:00', 7, 16, '2016-03-10 11:46:37'),
(22, '0000-00-00', '12:00:00', 1, 16, '2016-03-10 11:52:02'),
(23, '0000-00-00', '12:00:00', 1, 16, '2016-03-10 12:00:48'),
(24, '2017-01-01', '12:00:00', 1, 19, '2017-06-16 08:39:38'),
(25, '2017-01-01', '12:00:00', 1, 19, '2017-06-19 01:16:43'),
(26, '2017-01-01', '12:00:00', 1, 19, '2017-06-19 21:33:19'),
(27, '2017-01-01', '12:00:00', 1, 19, '2017-06-19 22:55:22'),
(28, '2017-01-01', '12:00:00', 1, 19, '2017-06-19 22:55:34');

-- --------------------------------------------------------

--
-- Structure de la table `meal`
--

CREATE TABLE `3w_meal` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Photo` varchar(30) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `QuantityInStock` tinyint(4) NOT NULL,
  `BuyPrice` double NOT NULL,
  `SalePrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `meal`
--

INSERT INTO `3w_meal` (`Id`, `Name`, `Photo`, `Description`, `QuantityInStock`, `BuyPrice`, `SalePrice`) VALUES
(1, 'Coca-Cola', 'coca.jpg', 'Mmmm, le Coca-Cola avec 10 morceaux de sucres et tout plein de caféine !', 72, 0.6, 3),
(2, 'Bagel Thon', 'bagel_thon.jpg', 'Notre bagel est constitué d\'un pain moelleux avec des grains de sésame et du thon albacore, accompagné de feuilles de salade fraîche du jour  et d\'une sauce renversante :-)', 18, 2.75, 5.5),
(3, 'Bacon Cheeseburger', 'bacon_cheeseburger.jpg', 'Ce délicieux cheeseburger contient un steak haché viande française de 150g ainsi que d\'un buns grillé juste comme il faut, le tout accompagné de frites fraîches maison !', 14, 6, 12.5),
(4, 'Carrot Cake', 'carrot_cake.jpg', 'Le carrot cake maison ravira les plus gourmands et les puristes : tous les ingrédients sont naturels !\r\nÀ consommer sans modération', 9, 3, 6.75),
(5, 'Donut Chocolat', 'chocolate_donut.jpg', 'Les donuts sont fabriqués le matin même et sont recouvert d\'une délicieuse sauce au chocolat !', 16, 3, 6.2),
(6, 'Dr. Pepper', 'drpepper.jpg', 'Son goût sucré avec de l\'amande vous ravira !', 53, 0.5, 2.9);

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE `3w_order` (
  `Id` int(11) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `TotalAmount` double DEFAULT NULL,
  `TaxRate` float DEFAULT NULL,
  `TaxAmount` double DEFAULT NULL,
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CompleteTimestamp` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `order`
--

INSERT INTO `3w_order` (`Id`, `User_Id`, `TotalAmount`, `TaxRate`, `TaxAmount`, `CreationTimestamp`, `CompleteTimestamp`) VALUES
(2, 13, 4163, 20, 832.6, '2016-03-16 16:53:13', '2016-03-16 16:53:13'),
(3, 13, 4163, 20, 832.6, '2016-03-16 16:55:16', '2016-03-16 16:55:16'),
(4, 19, 6, 20, 1.2, '2017-06-19 20:23:51', '2017-06-19 20:23:51'),
(5, 19, 69, 20, 13.8, '2017-06-19 20:47:31', '2017-06-19 20:47:31'),
(6, 19, 94, 20, 18.8, '2017-06-19 21:33:27', '2017-06-19 21:33:27'),
(7, 19, 6, 20, 1.2, '2017-06-19 22:44:31', '2017-06-19 22:44:31'),
(8, 19, 6, 20, 1.2, '2017-06-19 22:44:34', '2017-06-19 22:44:34'),
(9, 19, 6, 20, 1.2, '2017-06-19 22:44:50', '2017-06-19 22:44:50'),
(10, 19, 21, 20, 4.2, '2017-06-19 23:01:16', '2017-06-19 23:01:16'),
(12, 18, NULL, NULL, NULL, '2017-06-27 11:38:25', '2017-06-27 11:38:25'),
(13, 18, 29.5, NULL, NULL, '2017-06-27 11:41:42', '2017-06-27 11:41:42'),
(14, 18, 29.5, 20, NULL, '2017-06-27 11:53:18', '2017-06-27 11:53:18'),
(15, 18, 29.5, 20, NULL, '2017-06-27 12:15:01', '2017-06-27 12:15:01'),
(16, 18, 29.5, 20, NULL, '2017-06-27 12:15:23', '2017-06-27 12:15:23'),
(17, 18, 29.5, 20, NULL, '2017-06-27 12:15:44', '2017-06-27 12:15:44'),
(18, 18, 29.5, 20, NULL, '2017-06-27 12:16:24', '2017-06-27 12:16:24'),
(19, 18, 29.5, 20, NULL, '2017-06-27 12:16:53', '2017-06-27 12:16:53'),
(20, 18, 29.5, 20, NULL, '2017-06-27 12:19:57', '2017-06-27 12:19:57'),
(21, 18, 29.5, 20, NULL, '2017-06-27 12:20:17', '2017-06-27 12:20:17'),
(22, 21, 29.5, 20, NULL, '2017-06-27 12:44:02', '2017-06-27 12:44:02'),
(23, 23, 29.5, 20, NULL, '2017-06-27 12:53:15', '2017-06-27 12:53:15'),
(24, 25, 29.5, 20, NULL, '2017-06-27 12:53:45', '2017-06-27 12:53:45'),
(25, 26, 44.2, 20, NULL, '2017-06-27 13:25:51', '2017-06-27 13:25:51'),
(26, 27, 3, 20, NULL, '2017-06-27 13:57:25', '2017-06-27 13:57:25'),
(27, 28, 62.5, 20, NULL, '2017-06-27 17:20:02', '2017-06-27 17:20:02'),
(28, 29, 25, 20, NULL, '2017-06-27 17:26:24', '2017-06-27 17:26:24');

-- --------------------------------------------------------

--
-- Structure de la table `orderline`
--

CREATE TABLE `3w_orderline` (
  `Id` int(11) NOT NULL,
  `QuantityOrdered` int(4) NOT NULL,
  `Meal_Id` int(11) NOT NULL,
  `Order_Id` int(11) NOT NULL,
  `PriceEach` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `orderline`
--

INSERT INTO `3w_orderline` (`Id`, `QuantityOrdered`, `Meal_Id`, `Order_Id`, `PriceEach`) VALUES
(2, 15, 1, 2, 3),
(3, 5, 3, 2, 12.5),
(6, 6, 4, 2, 6.75),
(7, 15, 1, 3, 3),
(8, 5, 3, 3, 12.5),
(11, 6, 4, 3, 6.75),
(12, 2, 1, 4, 3),
(13, 23, 1, 5, 3),
(14, 23, 1, 6, 3),
(15, 2, 3, 6, 12.5),
(16, 2, 1, 7, 3),
(17, 2, 1, 8, 3),
(18, 2, 1, 9, 3),
(19, 7, 1, 10, 3),
(20, 2, 1, 21, 3),
(21, 2, 2, 21, 5.5),
(22, 1, 3, 21, 12.5),
(23, 2, 1, 22, 3),
(24, 2, 2, 22, 5.5),
(25, 1, 3, 22, 12.5),
(26, 2, 1, 23, 3),
(27, 2, 2, 23, 5.5),
(28, 1, 3, 23, 12.5),
(29, 2, 1, 24, 3),
(30, 2, 2, 24, 5.5),
(31, 1, 3, 24, 12.5),
(32, 3, 1, 25, 3),
(33, 3, 2, 25, 5.5),
(34, 1, 3, 25, 12.5),
(35, 1, 5, 25, 6.2),
(36, 1, 1, 26, 3),
(37, 5, 3, 27, 12.5),
(38, 2, 3, 28, 12.5);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `3w_user` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(40) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(64) NOT NULL,
  `Address` varchar(250) NOT NULL,
  `City` varchar(40) NOT NULL,
  `ZipCode` char(5) NOT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `CreationTimestamp` timestamp DEFAULT CURRENT_TIMESTAMP,
  `LastLoginTimestamp` timestamp
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `3w_user` (`Id`, `FirstName`, `LastName`, `Email`, `Password`, `Address`, `City`, `ZipCode`, `Country`, `CreationTimestamp`, `LastLoginTimestamp`) VALUES
(13, 'Lany', 'Drak', 'lanydrak@gmail.com', '$2y$11$da49ee0166983095e516dOBi2NgIUK/LtPMI7nJz6grrrX458Ak4i', '4 rue Robert Aylé', 'Asnières-sur-Seine', '92600', 'France', '2016-03-04 10:54:16', NULL),
(14, 'Yann', 'Amarre', 'yanamarre@laposte.fr', '$2y$11$cafed1e6068a72ed41ebbu70q8kDo3bwl1UlLjfhLRLmYeMtWn3Gm', '18 rue Du Ralbolle', 'Nice', '06000', 'France', '2016-03-08 14:50:01', NULL),
(15, 'Jean', '', 'jean@yahoo.fr', '$2y$11$2f7b7170d65112f5957e8ez7CDymXyJ6gF4Mj9eRO.DBqeDW4wzey', '', '', '', '', '2016-03-08 15:07:47', NULL),
(16, 'Vincent', 'Tim', '20cent@caramail.com', '$2y$11$244ed51efc8ad70ca1e54O99tttnqTySf.cWMNRJZr4zJue933dCW', '15 rue Victor Hugo', 'Poitiers', '86000', 'France', '2016-03-10 11:45:10', NULL),
(17, 'Marie-Odile2', 'Duchemin', 'mo@coucou', '$2y$11$441546026f24de3bbbd01eJ4RaWveSpe9K7GsylqhKUwjch80oaY.', 'btrsbtrzesgbtersbvtersv', 'vfreqzfvrevre', 'AAAAA', 'cfrsqezvfreqgresq', '2016-03-10 12:05:00', NULL),
(18, 'Michel', 'Moustache', 'michel.moustache@csvs', '$2y$11$b3e71948269136c994facew9ccZNQcpiWWYaZMRm8SzWrHYru//EO', 'vfqdsvreqsvrsevres', 'vresvresvres', 'AAAAA', 'vfdsvrfqsevrfqse', '2016-03-10 12:07:02', NULL),
(19, 'T', 'Ch', 'admin', '$2y$11$04798e03f080838d6c9a2e6JFGxTTCKn39Sneztes5iyNnSeQHqO6', 'dsds', 'jkjk', '9898', NULL, '2017-06-16 08:38:17', NULL),
(20, 'Roger', 'Jacques', 'truc@truc.com', 'pass', 'aaaa', 'City', '45455', 'France', '2017-06-27 12:38:51', '2017-06-27 12:38:51'),
(21, 'dsd', 'sds', 'ete@gege.fr', 'sd', 'dsd', 'sdsd', 'sd', NULL, '2017-06-27 12:44:02', '2017-06-27 12:44:02'),
(23, 'dsd', 'sds', 'ete@gfege.fr', '', 'dsd', 'sdsd', 'sd', NULL, '2017-06-27 12:53:15', '2017-06-27 12:53:15'),
(25, 'dsd', 'sds', 'dete@gfege.fr', '', 'dsd', 'sdsd', 'sd', NULL, '2017-06-27 12:53:45', '2017-06-27 12:53:45'),
(26, 'Blab', 'djjsj', 'tetete@shshs.fr', 'fdhsjkhfdkjh', 'jkdjskdj', 'dsdsds', '4545', NULL, '2017-06-27 13:25:51', '2017-06-27 13:25:51'),
(27, 'fdd', 'fdfdf', 'gfgf@hghjg.grgr', 'grt', 'dfd', 'fvfgf', '55', NULL, '2017-06-27 13:57:25', '2017-06-27 13:57:25'),
(28, '', '', '', '', '', '', '', NULL, '2017-06-27 17:20:02', '2017-06-27 17:20:02'),
(29, 'Jean', 'Pierre', 'email@email.com', 'ette', 'adsds dsds', 'Gtetets', '56565', NULL, '2017-06-27 17:26:24', '2017-06-27 17:26:24');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `booking`
--
ALTER TABLE `3w_booking`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Index pour la table `meal`
--
ALTER TABLE `3w_meal`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `order`
--
ALTER TABLE `3w_order`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Index pour la table `orderline`
--
ALTER TABLE `3w_orderline`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UniciteMealOrder` (`Meal_Id`,`Order_Id`),
  ADD KEY `Meal_Id` (`Meal_Id`),
  ADD KEY `Order_Id` (`Order_Id`);

--
-- Index pour la table `user`
--
ALTER TABLE `3w_user`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `booking`
--
ALTER TABLE `3w_booking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `meal`
--
ALTER TABLE `3w_meal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `order`
--
ALTER TABLE `3w_order`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `orderline`
--
ALTER TABLE `3w_orderline`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `3w_user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `booking`
--
ALTER TABLE `3w_booking`
  ADD CONSTRAINT `Booking_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `3w_user` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `order`
--
ALTER TABLE `3w_order`
  ADD CONSTRAINT `Order_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `3w_user` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `orderline`
--
ALTER TABLE `3w_orderline`
  ADD CONSTRAINT `OrderLine_ibfk_1` FOREIGN KEY (`Meal_Id`) REFERENCES `3w_meal` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `OrderLine_ibfk_2` FOREIGN KEY (`Order_Id`) REFERENCES `3w_order` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
