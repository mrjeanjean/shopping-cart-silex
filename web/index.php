<?php
use App\RestoApp;

$loader = require_once __DIR__ . '/../vendor/autoload.php';

$app = new RestoApp();

$app->run();